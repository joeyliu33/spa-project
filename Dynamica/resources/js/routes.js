
import Home from './components/Home.vue'
import Add from './components/Add.vue';

export const routes = [
    {
        name: 'home',
        path: '/',
        component: Home
    },
    {
        name: 'add',
        path: 'add',
        Component: Add
    }
];