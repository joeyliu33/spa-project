require('./bootstrap');

window.Vue = require('vue');

import Vue from 'vue';
import VueRouter from 'vue-router';
import {routes} from './routes';
import App from './App.vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap';

Vue.use(VueRouter);

const router = new VueRouter({
    routes
});

var one = new Vue({
    el: '#app',
    render: h => h(App),
    router
});
